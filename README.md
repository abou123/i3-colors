# i3-colors

Simple Python i3 colors manager.<br />
**Install/Usage:**

      git clone https://gitlab.com/manzerbredes/i3-colors
      ./i3-colors/src/i3-colors.py -h
    
**Notes:** Some of the themes come from [i3-style project](https://github.com/altdesktop/i3-style) and [j4-make-config](https://github.com/okraits/j4-make-config)

### Screenshots

alphare:
![alphare](themes/alphare.jpg)

archlinux:
![archlinux](themes/archlinux.jpg)

base16-tomorrow:
![base16-tomorrow](themes/base16-tomorrow.jpg)

debian:
![debian](themes/debian.jpg)

deep-purple:
![deep-purple](themes/deep-purple.jpg)

default:
![default](themes/default.jpg)

flat-gray:
![flat-gray](themes/flat-gray.jpg)

gruvbox:
![gruvbox](themes/gruvbox.jpg)

icelines:
![icelines](themes/icelines.jpg)

lime:
![lime](themes/lime.jpg)

mate:
![mate](themes/mate.jpg)

oceanic-next:
![oceanic-next](themes/oceanic-next.jpg)

okraits:
![okraits](themes/okraits.jpg)

purple:
![purple](themes/purple.jpg)

seti:
![seti](themes/seti.jpg)

slate:
![slate](themes/slate.jpg)

solarized:
![solarized](themes/solarized.jpg)

tomorrow-night-80s:
![tomorrow-night-80s](themes/tomorrow-night-80s.jpg)

ubuntu:
![ubuntu](themes/ubuntu.jpg)
